// November 12, 2013
/*
    NOTE TO USER: Uncomment/Comment two sensor equal behaviour based on the course.
                  All 3 courses are in this file.
*/
                  
#include "UW_eng.h"

void main() {
  initialize(); 
  int const Power = 30; // modify as needed if batteries or fuel cells
  int const delay = 10; 

  // wait for button press to start
  while (getSensor(BUTTON) == 0);
  while (getSensor(BUTTON) == 1);
  
  while(getSensor(BUTTON) == 0)
  {
      if (getSensor(BUMPER) == 1)
      {
         setMotor(MOTOR_A, -Power);
         setMotor(MOTOR_B, -Power);
         wait1Msec(750);
         setMotor(MOTOR_A, -Power);
         setMotor(MOTOR_B, Power); 
         wait1Msec(750);
         setMotor(MOTOR_A, Power);
         setMotor(MOTOR_B, Power);
      }
      setMotor(MOTOR_A, Power);
      setMotor(MOTOR_B, Power);

      if((getSensor(REFLECT_1) > 100)&&(getSensor(REFLECT_2) < 100))
      {
        setMotor(MOTOR_B, -Power);
        wait1Msec(delay);
        while(getSensor(REFLECT_2) < 100);
        setMotor(MOTOR_B, Power);
       }

      if((getSensor(REFLECT_2) > 100) && (getSensor(REFLECT_1) < 100))
      {
        setMotor(MOTOR_A, -Power);
        wait1Msec(delay);
        while(getSensor(REFLECT_1) < 75);
        setMotor(MOTOR_A, Power);
      }

    /*if((getSensor(REFLECT_2) < 100) && (getSensor(REFLECT_1) < 100))
      {
        setMotor(MOTOR_A, -Power); // turn right for cource B 
        wait1Msec(delay);
        while(getSensor(REFLECT_2) > 100);
        setMotor(MOTOR_A, Power);
      }*/
    
      if((getSensor(REFLECT_2) < 100) && (getSensor(REFLECT_1) < 100))
      {
        setMotor(MOTOR_A, Power); // Go straight in course C
        setMotor(MOTOR_B, Power);
        while((getSensor(REFLECT_2) < 100) && (getSensor(REFLECT_1) < 100));
      }  
  }
  setMotor(MOTOR_A, 0);
  setMotor(MOTOR_B, 0);
}
