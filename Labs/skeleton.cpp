#include "UW_eng.h"

void main(void)
{
    initialize();
    
    while (getSensor(BUTTON) == 0);
    while (getSensor(BUTTON) == 1);
    
    LEDon(RED_LED);
    LEDon(GREEN_LED);
    wait1Sec(2);
    
    LEDoff(RED_LED);
    LEDoff(GREEN_LED);
}