#include "UW_eng.h"

void main(void)
{
    initialize();
    
    while (getSensor(BUTTON) == 0);
    while (getSensor(BUTTON) == 1);
    for (int i = 0; i < 8; i++)
    {
        LEDon(RED_LED);
        wait10Msec(50);
        LEDoff(RED_LED);
        LEDon(GREEN_LED);
        wait10Msec(50);
        LEDoff(GREEN_LED);
    }
}