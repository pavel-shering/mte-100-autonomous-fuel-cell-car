#include "UW_eng.h"

void main()
{
    initialize(); 
    while (getSensor(BUTTON) == 0);
    while (getSensor(BUTTON) == 1);
    setMotor(MOTOR_A, 50);
    setMotor(MOTOR_B, 50);

    while(getSensor(BUMPER) == 0)
    { 
      if(getSensor(REFLECT_1) < 100) {
        setMotor(MOTOR_B, -50);
        while(getSensor(REFLECT_1) > 100);
        setMotor(MOTOR_B, 50);
      }

      if(getSensor(REFLECT_2) < 100) {
        setMotor(MOTOR_A, -50);
        while(getSensor(REFLECT_1) > 100);
        setMotor(MOTOR_A, 50);
      }
    }
}
